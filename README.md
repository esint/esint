# Emerging Systems Integration Provider [ESINT](es-int)


Email: esint.provider@gmail.com

Database: [Customers.db](https://bitbucket.org/esint/esint/src/master/customers.db) (sqlite)

Spreadsheet: [Ledger.sxls](https://bitbucket.org/esint/esint/src/master/ledger.sxls) (excel)

Presentation: https://buglouse.bitbucket.io/esint/
